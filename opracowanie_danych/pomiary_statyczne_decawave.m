
grid on
hold on

rysuj_tor()
rysuj_kotwice()

plot(x68mb_DW_static_A_x, x68mb_DW_static_A_y, 'm.');
plot(x68mb_DW_static_B_x, x68mb_DW_static_B_y, 'm.');
plot(x68mb_DW_static_C_x, x68mb_DW_static_C_y, 'm.');
plot(x68mb_DW_static_D_x, x68mb_DW_static_D_y, 'm.');
plot(x68mb_DW_static_E_x, x68mb_DW_static_E_y, 'm.');
plot(x68mb_DW_static_F_x, x68mb_DW_static_F_y, 'm.');
plot(x68mb_DW_static_G_x, x68mb_DW_static_G_y, 'm.');
plot(x68mb_DW_static_H_x, x68mb_DW_static_H_y, 'm.');
plot(x68mb_DW_static_I_x, x68mb_DW_static_I_y, 'm.');
plot(x68mb_DW_static_J_x, x68mb_DW_static_J_y, 'm.');
plot(x68mb_DW_static_K_x, x68mb_DW_static_K_y, 'm.');
plot(x68mb_DW_static_L_x, x68mb_DW_static_L_y, 'm.');
plot(x68mb_DW_static_M_x, x68mb_DW_static_M_y, 'm.');
plot(x68mb_DW_static_N_x, x68mb_DW_static_N_y, 'm.');
plot(x68mb_DW_static_O_x, x68mb_DW_static_O_y, 'm.');
plot(x68mb_DW_static_P_x, x68mb_DW_static_P_y, 'm.');
plot(x68mb_DW_static_R_x, x68mb_DW_static_R_y, 'm.');
plot(x68mb_DW_static_S_x, x68mb_DW_static_S_y, 'm.');
plot(x68mb_DW_static_T_x, x68mb_DW_static_T_y, 'm.');
hold off

axis equal;
axis([-0.5 8 -0.5 6])
xlabel('Odleg?o?? [m]') 
ylabel('Odleg?o?? [m]') 