% Wyznacza pozycj? obiektu na podstawie po?o?enia kotwic i ich odleg?o?ci od 
% obiektu. Dzia?a dla systemu decawave.
% Oblicza najmniejsz? odleg?o?? mi?dzy torem, a punktem pomiarowym.
% Do policzenia cdfu i histogramu s?uzy funkcja cdfhist.
% range to wczytane z pliku odleglosci od ka?dej z kotwic, na przyk?ad 
% z pliku 110kb_ch5_dynamic_decawave_cords pierwsze trzy kolumny

range = [range1, range2, range3]

A0 = [0, 0];
A1 = [0, 4.131];
A2 = [6.0, 2.048];

for i = 1: size(range, 1)
    if range(i,1) <= 0
        continue
    end
    if range(i,2) <= 0
        continue
    end
    if range(i,3) <= 0
        continue
    end
    range(i, :)
    [x(i), y(i)] = wyznacz_punkt(range(i, :));
end


grid on
hold on
punkty = rysuj_tor();

clear min_dist
for i=1:length(x)
    min_dist(i) = 1000000000000;
    for j=1:size(punkty, 2)
        dist = sqrt((x(i) - punkty(1, j))^2 + (y(i) - punkty(2, j))^2);
        if dist < min_dist(i)
            min_dist(i) = dist;
        end
    end
end


rysuj_kotwice()
plot(x + 1, y + 1, 'm.')

xlabel('Distance [m]', 'FontSize', 16)
ylabel('Distance [m]', 'FontSize', 16)

