% Por�wnanie zmierzonych pr�bek na torze dla DecaWave i Pozyx.

grid on
hold on

rysuj_tor()
rysuj_kotwice()

h = zeros(1,2);

h(1) = plot(x, y, 'm.');
h(2) = plot(P_NONE_x / 1000 + 1, P_NONE_y / 1000 + 1, 'b.');

legend(h(1:2),...
    'DecaWave (DW1000)',...
    'Pozyx (DW1001)');

hold off

axis equal;
axis([-0.5 8 -0.5 6])
xlabel('Distance [m]') 
ylabel('Distance [m]') 