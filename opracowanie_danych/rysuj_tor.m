%% TOR POMIAROWY

function punkty = rysuj_tor
    line_width = 2;

    % Duzy luk po lewej stronie
    luk_duzy = linspace( pi/2, -pi/2, 100);
    luk_duzy_R = 2;
    luk_duzy_x = -luk_duzy_R * cos(luk_duzy) + 3;
    luk_duzy_y = luk_duzy_R * sin(luk_duzy) + 3;
    plot(luk_duzy_x, luk_duzy_y, 'k-', 'LineWidth', line_width);

    %linia gorna
%     linia_gorna_x = [3 4];
%     linia_gorna_y = [5 5];
    linia_gorna_x = 3:0.001:4;
    linia_gorna_y = repelem([5], size(linia_gorna_x, 2));
    plot(linia_gorna_x, linia_gorna_y, 'k-', 'LineWidth', line_width)

    % Luk po prawej stronie u gory
    luk_pg = linspace( pi/2, 0, 100);
    luk_pg_R = 1;
    luk_pg_x = luk_pg_R * cos(luk_pg) + 4;
    luk_pg_y = luk_pg_R * sin(luk_pg) + 4;
    plot(luk_pg_x, luk_pg_y, 'k-', 'LineWidth', line_width);

    % Linia pionowa po prawej u gory
%     linia_pg_x = [5 5];
%     linia_pg_y = [4 3];
    linia_pg_y = 3:0.001:4;
    linia_pg_x = repelem([5], size(linia_pg_y, 2));
    plot(linia_pg_x, linia_pg_y, 'k-', 'LineWidth', line_width)

    % Luk po prawej stronie na srodku
    luk_ps = linspace( pi/2, 0, 100);
    luk_ps_R = 1;
    luk_ps_x = -luk_ps_R * cos(luk_ps) + 6;
    luk_ps_y = -luk_ps_R * sin(luk_ps) + 3;
    plot(luk_ps_x, luk_ps_y, 'k-', 'LineWidth', line_width);

    % Linia pozioma na gorze cypelka
%     linia_gc_x = [6 7];
%     linia_gc_y = [2 2];
    linia_gc_x = 6:0.001:7;
    linia_gc_y = repelem([2], size(linia_gc_x, 2));

    plot(linia_gc_x, linia_gc_y, 'k-', 'LineWidth', line_width)

    % Maly luk po prawej na dole
    luk_maly = linspace( pi/2, -pi/2, 100);
    luk_maly_R = 0.5;
    luk_maly_x = luk_maly_R * cos(luk_maly) + 7;
    luk_maly_y = luk_maly_R * sin(luk_maly) + 1.5;
    plot(luk_maly_x, luk_maly_y, 'k-', 'LineWidth', line_width);

    % Linia pozioma na dole
%     linia_gc_x = [3 7];
%     linia_gc_y = [1 1];
    linia_gc_x = 3:0.001:7;
    linia_gc_y = repelem([1], size(linia_gc_x, 2));
    plot(linia_gc_x, linia_gc_y, 'k-', 'LineWidth', line_width)
    
    punktyX = [luk_duzy_x, linia_gorna_x, luk_pg_x, linia_pg_x, luk_ps_x,...
               linia_gc_x, luk_maly_x, linia_gc_x];
    punktyY = [luk_duzy_y, linia_gorna_y, luk_pg_y, linia_pg_y, luk_ps_y,...
               linia_gc_y, luk_maly_y, linia_gc_y];
           
    punkty = [punktyX - 1; punktyY - 1];
    
     
end