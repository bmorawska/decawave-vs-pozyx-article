% Tworzy histogram na podstawie odleg?o?ci pomi?dzy torem, a punktami 
% pomiarowymi. Nast?pnie na jego podstawie generuje wykres CDF.
% min_dist - dane do histogramu
% normalized - dane do CDF

a = hist(min_dist, 100);

sum = 0;
for i=1:length(a)
    sum = sum + a(i);
    c(i) = sum;
end

normalized = c / max(c);

cdf_decawave_110kb_MEDIAN21 = normalized;
hist_decawave_110kb_MEDIAN21 = min_dist;

hist(min_dist, 100);

figure
grid on

h = plot(normalized, 'r-', 'LineWidth', 2);

xlabel('Distance [cm]', 'FontSize', 16);
ylabel('CDF', 'FontSize', 16);
