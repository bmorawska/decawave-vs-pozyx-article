% Por�wnanie filtr�w medianowych wbudowanych w aplikacje Pozyx.

grid on
hold on

rysuj_tor()
rysuj_kotwice()

delimiter = ',';

h = zeros(1,4);

h(1) = plot(P_NONE_x(5:end,:) / 1000 + 1, P_NONE_y(5:end,:) / 1000 + 1, 'k-');
h(2) = plot(P_MEDIAN_5_x(5:end,:) / 1000 + 1, P_MEDIAN_5_y(5:end,:) / 1000 + 1, 'g-');
h(3) = plot(P_MEDIAN_15_x(5:end,:) / 1000 + 1, P_MEDIAN_15_y(5:end,:) / 1000 + 1, 'r-');

legend(h(1:3),...
    'przebieg bez filtru',...
    'filtr medianowy (5)',...
    'filtr medianowy (15)');

hold off

axis equal;
axis([-0.5 8 -0.5 6])
xlabel('Odleg?o?? [m]') 
ylabel('Odleg?o?? [m]') 


