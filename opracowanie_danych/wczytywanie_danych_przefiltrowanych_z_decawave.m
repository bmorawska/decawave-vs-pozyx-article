%% Wczytywanie danych przefiltrowanych z decawave
decawaveAVG50 = load('./wyniki_filtry/decawaveDaneDoFiltrow_results_alpha_50.csv', '-ascii');
decawaveAVG75 = load('./wyniki_filtry/decawaveDaneDoFiltrow_results_alpha_75.csv', '-ascii');
decawaveAVG80 = load('./wyniki_filtry/decawaveDaneDoFiltrow_results_alpha_80.csv', '-ascii');
decawaveAVG90 = load('./wyniki_filtry/decawaveDaneDoFiltrow_results_alpha_90.csv', '-ascii');
decawaveAVG95 = load('./wyniki_filtry/decawaveDaneDoFiltrow_results_alpha_95.csv', '-ascii');

decawaveAVG50x = decawaveAVG50(:, 3);
decawaveAVG50y = decawaveAVG50(:, 4);
clear decawaveAVG50;

decawaveAVG75x = decawaveAVG75(:, 3);
decawaveAVG75y = decawaveAVG75(:, 4);
clear decawaveAVG75;

decawaveAVG80x = decawaveAVG80(:, 3);
decawaveAVG80y = decawaveAVG80(:, 4);
clear decawaveAVG80;

decawaveAVG90x = decawaveAVG90(:, 3);
decawaveAVG90y = decawaveAVG90(:, 4);
clear decawaveAVG90;

decawaveAVG95x = decawaveAVG95(:, 3);
decawaveAVG95y = decawaveAVG95(:, 4);
clear decawaveAVG95;

decawaveKalman = load('./wyniki_filtry/decawaveDaneDoFiltrow_results_kalman.csv', '-ascii');

decawaveKalmanx = decawaveKalman(:, 3);
decawaveKalmany = decawaveKalman(:, 4);
clear decawaveKalman;

decawaveMEDIAN3 = load('./wyniki_filtry/decawaveDaneDoFiltrow_results_window_size_3.csv', '-ascii');
decawaveMEDIAN5 = load('./wyniki_filtry/decawaveDaneDoFiltrow_results_window_size_5.csv', '-ascii');
decawaveMEDIAN7 = load('./wyniki_filtry/decawaveDaneDoFiltrow_results_window_size_7.csv', '-ascii');
decawaveMEDIAN9 = load('./wyniki_filtry/decawaveDaneDoFiltrow_results_window_size_9.csv', '-ascii');
decawaveMEDIAN11 = load('./wyniki_filtry/decawaveDaneDoFiltrow_results_window_size_11.csv', '-ascii');
decawaveMEDIAN13 = load('./wyniki_filtry/decawaveDaneDoFiltrow_results_window_size_13.csv', '-ascii');
decawaveMEDIAN15 = load('./wyniki_filtry/decawaveDaneDoFiltrow_results_window_size_15.csv', '-ascii');
decawaveMEDIAN17 = load('./wyniki_filtry/decawaveDaneDoFiltrow_results_window_size_17.csv', '-ascii');
decawaveMEDIAN19 = load('./wyniki_filtry/decawaveDaneDoFiltrow_results_window_size_19.csv', '-ascii');
decawaveMEDIAN21 = load('./wyniki_filtry/decawaveDaneDoFiltrow_results_window_size_21.csv', '-ascii');

decawaveMEDIAN3x = decawaveMEDIAN3(:, 3);
decawaveMEDIAN3y = decawaveMEDIAN3(:, 4);
clear decawaveMEDIAN3;

decawaveMEDIAN5x = decawaveMEDIAN5(:, 3);
decawaveMEDIAN5y = decawaveMEDIAN5(:, 4);
clear decawaveMEDIAN5;

decawaveMEDIAN7x = decawaveMEDIAN7(:, 3);
decawaveMEDIAN7y = decawaveMEDIAN7(:, 4);
clear decawaveMEDIAN7;

decawaveMEDIAN9x = decawaveMEDIAN9(:, 3);
decawaveMEDIAN9y = decawaveMEDIAN9(:, 4);
clear decawaveMEDIAN9;

decawaveMEDIAN11x = decawaveMEDIAN11(:, 3);
decawaveMEDIAN11y = decawaveMEDIAN11(:, 4);
clear decawaveMEDIAN11;

decawaveMEDIAN13x = decawaveMEDIAN13(:, 3);
decawaveMEDIAN13y = decawaveMEDIAN13(:, 4);
clear decawaveMEDIAN13;

decawaveMEDIAN15x = decawaveMEDIAN15(:, 3);
decawaveMEDIAN15y = decawaveMEDIAN15(:, 4);
clear decawaveMEDIAN15;

decawaveMEDIAN17x = decawaveMEDIAN17(:, 3);
decawaveMEDIAN17y = decawaveMEDIAN17(:, 4);
clear decawaveMEDIAN17;

decawaveMEDIAN19x = decawaveMEDIAN19(:, 3);
decawaveMEDIAN19y = decawaveMEDIAN19(:, 4);
clear decawaveMEDIAN19;

decawaveMEDIAN21x = decawaveMEDIAN21(:, 3);
decawaveMEDIAN21y = decawaveMEDIAN21(:, 4);
clear decawaveMEDIAN21;

%% Wczytywanie danych przefiltrowanych z pozyx
pozyxAVG50 = load('./wyniki_filtry/pozyxDaneDoFiltrow_results_alpha_50.csv', '-ascii');
pozyxAVG75 = load('./wyniki_filtry/pozyxDaneDoFiltrow_results_alpha_75.csv', '-ascii');
pozyxAVG80 = load('./wyniki_filtry/pozyxDaneDoFiltrow_results_alpha_80.csv', '-ascii');
pozyxAVG90 = load('./wyniki_filtry/pozyxDaneDoFiltrow_results_alpha_90.csv', '-ascii');
pozyxAVG95 = load('./wyniki_filtry/pozyxDaneDoFiltrow_results_alpha_95.csv', '-ascii');

pozyxAVG50x = pozyxAVG50(:, 3);
pozyxAVG50y = pozyxAVG50(:, 4);
clear pozyxAVG50;

pozyxAVG75x = pozyxAVG75(:, 3);
pozyxAVG75y = pozyxAVG75(:, 4);
clear pozyxAVG75;

pozyxAVG80x = pozyxAVG80(:, 3);
pozyxAVG80y = pozyxAVG80(:, 4);
clear pozyxAVG80;

pozyxAVG90x = pozyxAVG90(:, 3);
pozyxAVG90y = pozyxAVG90(:, 4);
clear pozyxAVG90;

pozyxAVG95x = pozyxAVG95(:, 3);
pozyxAVG95y = pozyxAVG95(:, 4);
clear pozyxAVG95;

pozyxKalman = load('./wyniki_filtry/pozyxDaneDoFiltrow_results_kalman.csv', '-ascii');

pozyxKalmanx = pozyxKalman(:, 3);
pozyxKalmany = pozyxKalman(:, 4);
clear pozyxKalman;

pozyxMEDIAN3 = load('./wyniki_filtry/pozyxDaneDoFiltrow_results_window_size_3.csv', '-ascii');
pozyxMEDIAN5 = load('./wyniki_filtry/pozyxDaneDoFiltrow_results_window_size_5.csv', '-ascii');
pozyxMEDIAN7 = load('./wyniki_filtry/pozyxDaneDoFiltrow_results_window_size_7.csv', '-ascii');
pozyxMEDIAN9 = load('./wyniki_filtry/pozyxDaneDoFiltrow_results_window_size_9.csv', '-ascii');
pozyxMEDIAN11 = load('./wyniki_filtry/pozyxDaneDoFiltrow_results_window_size_11.csv', '-ascii');
pozyxMEDIAN13 = load('./wyniki_filtry/pozyxDaneDoFiltrow_results_window_size_13.csv', '-ascii');
pozyxMEDIAN15 = load('./wyniki_filtry/pozyxDaneDoFiltrow_results_window_size_15.csv', '-ascii');
pozyxMEDIAN17 = load('./wyniki_filtry/pozyxDaneDoFiltrow_results_window_size_17.csv', '-ascii');
pozyxMEDIAN19 = load('./wyniki_filtry/pozyxDaneDoFiltrow_results_window_size_19.csv', '-ascii');
pozyxMEDIAN21 = load('./wyniki_filtry/pozyxDaneDoFiltrow_results_window_size_21.csv', '-ascii');

pozyxMEDIAN3x = pozyxMEDIAN3(:, 3);
pozyxMEDIAN3y = pozyxMEDIAN3(:, 4);
clear pozyxMEDIAN3;

pozyxMEDIAN5x = pozyxMEDIAN5(:, 3);
pozyxMEDIAN5y = pozyxMEDIAN5(:, 4);
clear pozyxMEDIAN5;

pozyxMEDIAN7x = pozyxMEDIAN7(:, 3);
pozyxMEDIAN7y = pozyxMEDIAN7(:, 4);
clear pozyxMEDIAN7;

pozyxMEDIAN9x = pozyxMEDIAN9(:, 3);
pozyxMEDIAN9y = pozyxMEDIAN9(:, 4);
clear pozyxMEDIAN9;

pozyxMEDIAN11x = pozyxMEDIAN11(:, 3);
pozyxMEDIAN11y = pozyxMEDIAN11(:, 4);
clear pozyxMEDIAN11;

pozyxMEDIAN13x = pozyxMEDIAN13(:, 3);
pozyxMEDIAN13y = pozyxMEDIAN13(:, 4);
clear pozyxMEDIAN13;

pozyxMEDIAN15x = pozyxMEDIAN15(:, 3);
pozyxMEDIAN15y = pozyxMEDIAN15(:, 4);
clear pozyxMEDIAN15;

pozyxMEDIAN17x = pozyxMEDIAN17(:, 3);
pozyxMEDIAN17y = pozyxMEDIAN17(:, 4);
clear pozyxMEDIAN17;

pozyxMEDIAN19x = pozyxMEDIAN19(:, 3);
pozyxMEDIAN19y = pozyxMEDIAN19(:, 4);
clear pozyxMEDIAN19;

pozyxMEDIAN21x = pozyxMEDIAN21(:, 3);
pozyxMEDIAN21y = pozyxMEDIAN21(:, 4);
clear pozyxMEDIAN21;


