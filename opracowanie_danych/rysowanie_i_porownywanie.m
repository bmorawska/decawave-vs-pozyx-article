% Skrypt zbiorczy do rysowania wykresow na podstawie wygenerowanych danych.
% 1. Porównanie filtrów u?redniaj?cych Pozyx
% 2. Porównanie na obrazku 2x2 pozyx i decawave
% 3. Porównanie statyczne pozyx i decawave
% 4. Porównanie dynamiczne bez filtracji pozyx i decawave.

hold on
grid on

%%
% plot(pozyx_cdf, 'r-', 'LineWidth', 2);
% plot(cdf_pozyx_110kb_AVG50, 'm-.', 'LineWidth', 2);
% plot(cdf_pozyx_110kb_AVG75, 'k:', 'LineWidth', 2.3);
% plot(cdf_pozyx_110kb_AVG95, 'b--', 'LineWidth', 2);
% 
% legend('no filter', ...
%        'ARMA filter (alpha = 0.5)',...
%        'ARMA filter (alpha = 0.75)',...
%        'ARMA filter (alpha = 0.95)')
% 
% xlabel('Distance [cm]', 'FontSize', 16)
% ylabel('CDF', 'FontSize', 16)

%%

% subplot(2,2,1)
% hold on
% grid on
% rysuj_tor();
% rysuj_kotwice();
% plot(x + 1, y + 1, 'r-');
% xlabel('Distance [m]', 'FontSize', 12)
% ylabel('Distance [m]', 'FontSize', 12)
% hold off
% title('DecaWave - no filter')
% 
% subplot(2,2,2)  
% hold on
% grid on
% rysuj_tor();
% rysuj_kotwice();
% plot(decawaveMEDIAN7x + 1, decawaveMEDIAN7y + 1, 'r-');   
% xlabel('Distance [m]', 'FontSize', 12)
% ylabel('Distance [m]', 'FontSize', 12)
% hold off
% title('DecaWave - median filter (window = 7)')
% 
% subplot(2,2,3)     
% hold on
% grid on
% rysuj_tor();
% rysuj_kotwice();
% plot(P_NONE_x / 1000 + 1,P_NONE_y / 1000 +  1, 'r-');   
% xlabel('Distance [m]', 'FontSize', 12)
% ylabel('Distance [m]', 'FontSize', 12)
% hold off
% title('Pozyx - no filter')
% 
% subplot(2,2,4)       
% hold on
% grid on
% rysuj_tor();
% rysuj_kotwice();
% plot(pozyxMEDIAN7x / 1000 + 1, pozyxMEDIAN7y / 1000 + 1, 'r-');
% xlabel('Distance [m]', 'FontSize', 12)
% ylabel('Distance [m]', 'FontSize', 12)
% hold off
% title('Median filter (window = 7)')

%%

% hold on
% grid on
% rysuj_tor();
% rysuj_kotwice();
% 
% 
% plot(P_static_A_x / 1000 + 1, P_static_A_y / 1000 + 1, 'bx');
% plot(P_static_B_x / 1000 + 1, P_static_B_y / 1000 + 1, 'bx');
% plot(P_static_C_x / 1000 + 1, P_static_C_y / 1000 + 1, 'bx');
% plot(P_static_D_x / 1000 + 1, P_static_D_y / 1000 + 1, 'bx');
% plot(P_static_E_x / 1000 + 1, P_static_E_y / 1000 + 1, 'bx');
% plot(P_static_F_x / 1000 + 1, P_static_F_y / 1000 + 1, 'bx');
% plot(P_static_G_x / 1000 + 1, P_static_G_y / 1000 + 1, 'bx');
% plot(P_static_H_x / 1000 + 1, P_static_H_y / 1000 + 1, 'bx');
% plot(P_static_I_x / 1000 + 1, P_static_I_y / 1000 + 1, 'bx');
% plot(P_static_J_x / 1000 + 1, P_static_J_y / 1000 + 1, 'bx');
% plot(P_static_K_x / 1000 + 1, P_static_K_y / 1000 + 1, 'bx');
% plot(P_static_L_x / 1000 + 1, P_static_L_y / 1000 + 1, 'bx');
% plot(P_static_M_x / 1000 + 1, P_static_M_y / 1000 + 1, 'bx');
% plot(P_static_N_x / 1000 + 1, P_static_N_y / 1000 + 1, 'bx');
% plot(P_static_O_x / 1000 + 1, P_static_O_y / 1000 + 1, 'bx');
% plot(P_static_P_x / 1000 + 1, P_static_P_y / 1000 + 1, 'bx');
% plot(P_static_R_x / 1000 + 1, P_static_R_y / 1000 + 1, 'bx');
% plot(P_static_S_x / 1000 + 1, P_static_S_y / 1000 + 1, 'bx');
% plot(P_static_T_x / 1000 + 1, P_static_T_y / 1000 + 1, 'bx');
% 
% 
% plot(A110x + 1, A110y + 1, 'r.')
% plot(B110x + 1, B110y + 1, 'r.')
% plot(C110x + 1, C110y + 1, 'r.')
% plot(D110x + 1, D110y + 1, 'r.')
% plot(E110x + 1, E110y + 1, 'r.')
% plot(F110x + 1, F110y + 1, 'r.')
% plot(G110x + 1, G110y + 1, 'r.')
% plot(H110x + 1, H110y + 1, 'r.')
% plot(I110x + 1, I110y + 1, 'r.')
% plot(J110x + 1, J110y + 1, 'r.')
% plot(K110x + 1, K110y + 1, 'r.')
% plot(L110x + 1, L110y + 1, 'r.')
% plot(M110x + 1, M110y + 1, 'r.')
% plot(N110x + 1, N110y + 1, 'r.')
% plot(O110x + 1, O110y + 1, 'r.')
% plot(P110x + 1, P110y + 1, 'r.')
% plot(R110x + 1, R110y + 1, 'r.')
% plot(S110x + 1, S110y + 1, 'r.')
% plot(T110x + 1, T110y + 1, 'r.')
% 
% legend('DecaWave (DW1000)', 'Pozyx (DW1001)')
% 
% xlabel('Distance [m]', 'FontSize', 16)
% ylabel('Distance [m]', 'FontSize', 16)
% axis equal;
% axis([-0.5 8 -0.5 6])

%%
hold on
grid on
rysuj_tor();
rysuj_kotwice();

plot(x + 1, y+ 1, 'r.')
plot(P_NONE_x / 1000 + 1, P_NONE_y / 1000 + 1, 'b.')

legend('DecaWave (DW1000)', 'Pozyx (DW1001)')
xlabel('Distance [m]', 'FontSize', 16)
ylabel('Distance [m]', 'FontSize', 16)


