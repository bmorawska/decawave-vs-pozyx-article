% Wyznacza najmniejsz? odleg?o?? pomi?dzy torem, a 
% konkretnym punktem pomiarowym dla decawave.
% min_dist - tablica z odleg?o?ciami

grid on
hold on
punkty = rysuj_tor();

x = decawaveMEDIAN21x;
y = decawaveMEDIAN21y;

clear min_dist
for i=1:length(x)
    min_dist(i) = 1000000000000;
    for j=1:size(punkty, 2)
        dist = sqrt((x(i) - punkty(1, j))^2 + (y(i) - punkty(2, j))^2);
        if dist < min_dist(i)
            min_dist(i) = dist;
        end
    end
end


rysuj_kotwice()
plot(x + 1, y + 1, 'm-')

xlabel('Distance [m]', 'FontSize', 16)
ylabel('Distance [m]', 'FontSize', 16)