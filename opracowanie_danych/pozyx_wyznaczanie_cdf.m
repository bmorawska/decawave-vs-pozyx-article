% Obliczanie najkr�tszej odleg?o?ci mi?dzy torem, a punktem pomiarowym 
% dla pozyxa.
% Wynik w min_dist.

grid on
hold on
punkty = rysuj_tor();

x = pozyxMEDIAN21x / 1000;
y = pozyxMEDIAN21y / 1000;

clear min_dist

iidx=0;
for i=1:length(x)
    if isnan(x(i)) || isnan(y(i))
        continue
    end
    iidx = iidx+1;
    min_dist(iidx) = 1000000000000;
    for j=1:size(punkty, 2)
        dist = sqrt((x(i) - punkty(1, j))^2 + (y(i) - punkty(2, j))^2);
        if isnan(dist)
            die
        end
        if dist < min_dist(iidx)
            min_dist(iidx) = dist;
        end
    end
end


rysuj_kotwice()
plot(x + 1, y + 1, 'm-')