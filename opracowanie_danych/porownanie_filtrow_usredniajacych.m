% Por�wnanie filtr�w u?redniaj?cych wbudowanych w aplikacje Pozyx.

grid on
hold on

rysuj_tor()
rysuj_kotwice()

delimiter = ',';

h = zeros(1,4);

%h(1) = plot(P_AVG_0_x(5:end,:) / 1000 + 1, P_AVG_0_y(5:end,:) / 1000 + 1, 'k-');
h(2) = plot(P_AVG_5_x(5:end,:) / 1000 + 1, P_AVG_5_y(5:end,:) / 1000 + 1, 'g-');
h(3) = plot(P_AVG_15_x(5:end,:) / 1000 + 1, P_AVG_15_y(5:end,:) / 1000 + 1, 'r-');

legend(h(2:3),...
    'filtr u?redniaj?cy (5)',...
    'filtr u?redniaj?cy (15)');

hold off

axis equal;
axis([-0.5 8 -0.5 6])
xlabel('Odleg?o?? [m]') 
ylabel('Odleg?o?? [m]') 


