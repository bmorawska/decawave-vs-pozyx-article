% Wyznaczanie pozyxji punktu na podstawie odleglosci od kotwicy.

function [pktx, pkty] = wyznacz_punkt(range)

%Wspolrzedne kotwic
A0 = [0, 0];
A1 = [0, 4.131];
A2 = [6.0, 2.048];

A = [A0; A1; A2];

pidx = 1;
for i = 1:3
    for j = i + 1:3
        if i ~= j
            [xout,yout] = circcirc(A(i,1),A(i,2),range(i),...
                                   A(j,1),A(j,2),range(j));
            if isnan(xout(1))
                point = licz_wierzcholek(A(i,:), range(i),...
                                         A(j,:), range(j));
                xout(1) = point(1);
                xout(2) = point(1);
                yout(1) = point(2);
                yout(2) = point(2);
                
            end
                
            p(pidx,1) = xout(1);
            p(pidx,2) = yout(1);
            pidx = pidx+1;
            p(pidx,1) = xout(2);
            p(pidx,2) = yout(2);
            pidx = pidx+1;
            
        end
    end
end

% tworzymy tr�j?ty

indeksy = [1, 3, 5; ...
           1, 3, 6; ...
           1, 4, 5; ...
           1, 4, 6; ...
           2, 3, 5; ...
           2, 3, 6; ...
           2, 4, 5; ...
           2, 4, 6];
       

min_pole = 1000000000;  
for i = 1:8
    xy = [p(indeksy(i,1),1), ...
          p(indeksy(i,2),1), ...
          p(indeksy(i,3),1)];
    srx(i) = mean(xy);
    ygreki = [p(indeksy(i,1),2), ...
              p(indeksy(i,2),2), ...
              p(indeksy(i,3),2)];
    sry(i) = mean(ygreki);
    pp(i) = polyarea(xy, ygreki);
    
    if pp(i) < min_pole
        min_pole = pp(i);
        pktx = srx(i);
        pkty = sry(i);
    end
end
