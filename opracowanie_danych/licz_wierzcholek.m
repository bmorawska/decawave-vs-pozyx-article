% Liczy wierzcholek tr�jk?ta, w kt�rym znajduje si? poszukiwany punkt
% w przypadku gdy okr?gi nie przecinaj? si?.
function point = licz_wierzcholek(a1, r1, a2, r2)

    vec_center_center = a1 - a2;
    norma = norm(vec_center_center);
    d = norma - r1 - r2;

    u = d / (r1 / r2 + 1);
    point = vec_center_center * ((u + r2) / norma) + a2;


