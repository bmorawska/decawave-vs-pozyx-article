% Por�wnanie filtr�w dolnoprzepustowych wbudowanych w aplikacje Pozyx.

grid on
hold on

rysuj_tor()
rysuj_kotwice()

delimiter = ',';

h = zeros(1,4);

h(1) = plot(P_LOW_PASS_0_x(5:end,:) / 1000 + 1, P_LOW_PASS_0_y(5:end,:) / 1000 + 1, 'k-');
h(2) = plot(P_LOW_PASS_5_x(5:end,:) / 1000 + 1, P_LOW_PASS_5_y(5:end,:) / 1000 + 1, 'g-');
h(3) = plot(P_LOW_PASS_15_x(5:end,:) / 1000 + 1, P_LOW_PASS_15_y(5:end,:) / 1000 + 1, 'r-');

legend(h(2:3),...
    'filtr dolnoprzepustowy (5)',...
    'filtr dolnoprzepustowy (15)');

hold off

axis equal;
axis([-0.5 8 -0.5 6])
xlabel('Odleg?o?? [m]') 
ylabel('Odleg?o?? [m]') 


