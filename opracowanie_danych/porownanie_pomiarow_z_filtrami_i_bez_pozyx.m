% Por�wnanie pozyxa bez filtr�w i z filtrami z ich aplikacji.

grid on
hold on

rysuj_tor()
rysuj_kotwice()

delimiter = ',';

h = zeros(1,4);

h(1) = plot(P_NONE_x(5:end,:) / 1000 + 1, P_NONE_y(5:end,:) / 1000 + 1, 'k-');
h(2) = plot(P_MEDIAN_15_x(5:end,:) / 1000 + 1, P_MEDIAN_15_y(5:end,:) / 1000 + 1, 'r-');
h(3) = plot(P_AVG_15_x(5:end,:) / 1000 + 1, P_AVG_15_y(5:end,:) / 1000 + 1, 'g-');
h(4) = plot(P_LOW_PASS_15_x(5:end,:) / 1000 + 1, P_LOW_PASS_15_y(5:end,:) / 1000 + 1, 'm-');

legend(h(1:4),...
    'przebieg bez filtru',...
    'filtr medianowy',...
    'filtr u?redniaj?cy',...
    'filtr dolnoprzepustowy');

hold off

axis equal;
axis([-0.5 8 -0.5 6])
xlabel('Distance [m]') 
ylabel('Distance [m]') 