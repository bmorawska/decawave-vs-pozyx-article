%% KOTWICE

function rysuj_kotwice
    A0x = 1; A0y = 1; 
    A1x = 1; A1y = 5;
    A2x = 7; A2y = 3;

    plot(A0x, A0y,'^', 'MarkerFaceColor', 'g', 'MarkerEdgeColor', 'k', 'MarkerSize', 12)
    plot(A1x, A1y,'^', 'MarkerFaceColor', 'g', 'MarkerEdgeColor', 'k', 'MarkerSize', 12)
    plot(A2x, A2y,'^', 'MarkerFaceColor', 'g', 'MarkerEdgeColor', 'k', 'MarkerSize', 12)
end