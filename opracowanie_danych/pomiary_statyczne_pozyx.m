% Rysuje na torze wszystkie pomiary statyczne wykonane przy pomocy pozyx.

grid on
hold on

rysuj_tor()
rysuj_kotwice()

plot(P_static_A_x / 1000 + 1, P_static_A_y / 1000 + 1, 'b.');
plot(P_static_B_x / 1000 + 1, P_static_B_y / 1000 + 1, 'b.');
plot(P_static_C_x / 1000 + 1, P_static_C_y / 1000 + 1, 'b.');
plot(P_static_D_x / 1000 + 1, P_static_D_y / 1000 + 1, 'b.');
plot(P_static_E_x / 1000 + 1, P_static_E_y / 1000 + 1, 'b.');
plot(P_static_F_x / 1000 + 1, P_static_F_y / 1000 + 1, 'b.');
plot(P_static_G_x / 1000 + 1, P_static_G_y / 1000 + 1, 'b.');
plot(P_static_H_x / 1000 + 1, P_static_H_y / 1000 + 1, 'b.');
plot(P_static_I_x / 1000 + 1, P_static_I_y / 1000 + 1, 'b.');
plot(P_static_J_x / 1000 + 1, P_static_J_y / 1000 + 1, 'b.');
plot(P_static_K_x / 1000 + 1, P_static_K_y / 1000 + 1, 'b.');
plot(P_static_L_x / 1000 + 1, P_static_L_y / 1000 + 1, 'b.');
plot(P_static_M_x / 1000 + 1, P_static_M_y / 1000 + 1, 'b.');
plot(P_static_N_x / 1000 + 1, P_static_N_y / 1000 + 1, 'b.');
plot(P_static_O_x / 1000 + 1, P_static_O_y / 1000 + 1, 'b.');
plot(P_static_P_x / 1000 + 1, P_static_P_y / 1000 + 1, 'b.');
plot(P_static_R_x / 1000 + 1, P_static_R_y / 1000 + 1, 'b.');
plot(P_static_S_x / 1000 + 1, P_static_S_y / 1000 + 1, 'b.');
plot(P_static_T_x / 1000 + 1, P_static_T_y / 1000 + 1, 'b.');

hold off

axis equal;
axis([-0.5 8 -0.5 6])
xlabel('Distance [m]', 'FontSize', 12)
ylabel('Distance [m]', 'FontSize', 12)