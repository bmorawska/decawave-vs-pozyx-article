%Porównanie statyczne decawave i pozyx razem.

grid on
hold on

plot(P_static_A_x / 1000 + 1, P_static_A_y / 1000 + 1, 'g.');
plot(A110x + 1, A110y + 1, 'm.');
rysuj_tor()
rysuj_kotwice()

plot(P_static_B_x / 1000 + 1, P_static_B_y / 1000 + 1, 'g.');
plot(P_static_C_x / 1000 + 1, P_static_C_y / 1000 + 1, 'g.');
plot(P_static_D_x / 1000 + 1, P_static_D_y / 1000 + 1, 'g.');
plot(P_static_E_x / 1000 + 1, P_static_E_y / 1000 + 1, 'g.');
plot(P_static_F_x / 1000 + 1, P_static_F_y / 1000 + 1, 'g.');
plot(P_static_G_x / 1000 + 1, P_static_G_y / 1000 + 1, 'g.');
plot(P_static_H_x / 1000 + 1, P_static_H_y / 1000 + 1, 'g.');
plot(P_static_I_x / 1000 + 1, P_static_I_y / 1000 + 1, 'g.');
plot(P_static_J_x / 1000 + 1, P_static_J_y / 1000 + 1, 'g.');
plot(P_static_K_x / 1000 + 1, P_static_K_y / 1000 + 1, 'g.');
plot(P_static_L_x / 1000 + 1, P_static_L_y / 1000 + 1, 'g.');
plot(P_static_M_x / 1000 + 1, P_static_M_y / 1000 + 1, 'g.');
plot(P_static_N_x / 1000 + 1, P_static_N_y / 1000 + 1, 'g.');
plot(P_static_O_x / 1000 + 1, P_static_O_y / 1000 + 1, 'g.');
plot(P_static_P_x / 1000 + 1, P_static_P_y / 1000 + 1, 'g.');
plot(P_static_R_x / 1000 + 1, P_static_R_y / 1000 + 1, 'g.');
plot(P_static_S_x / 1000 + 1, P_static_S_y / 1000 + 1, 'g.');
plot(P_static_T_x / 1000 + 1, P_static_T_y / 1000 + 1, 'g.');


plot(B110x + 1, B110y + 1, 'm.');
plot(C110x + 1, C110y + 1, 'm.');
plot(D110x + 1, D110y + 1, 'm.');
plot(E110x + 1, E110y + 1, 'm.');
plot(F110x + 1, F110y + 1, 'm.');
plot(G110x + 1, G110y + 1, 'm.');
plot(H110x + 1, H110y + 1, 'm.');
plot(I110x + 1, I110y + 1, 'm.');
plot(J110x + 1, J110y + 1, 'm.');
plot(K110x + 1, K110y + 1, 'm.');
plot(L110x + 1, L110y + 1, 'm.');
plot(M110x + 1, M110y + 1, 'm.');
plot(N110x + 1, N110y + 1, 'm.');
plot(O110x + 1, O110y + 1, 'm.');
plot(P110x + 1, P110y + 1, 'm.');
plot(R110x + 1, R110y + 1, 'm.');
plot(S110x + 1, S110y + 1, 'm.');
plot(T110x + 1, T110y + 1, 'm.');

legend('Pozyx(DW1001)', 'DecaWave(DW1000)')

hold off

axis equal;
axis([-0.5 8 -0.5 6])
xlabel('Distance [m]') 
ylabel('Distance [m]') 