#include <Wire.h>
#include <ZumoShield.h>

#define LED_PIN 13
#define MAX_SPEED 150

ZumoMotors motors;

void setup()
{
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, HIGH);
}

void turnLeft(short moveTime)
{
  //turn left
  for (int sec = 0; sec <= moveTime; sec++)
  {
    motors.setSpeeds(-MAX_SPEED, MAX_SPEED);
    delay(100);
  }

  motors.setSpeeds(0,0);
  delay(300);
}

void turnRight(short moveTime)
{
  //turn right
  for (int sec = 0; sec <= moveTime; sec++)
  {
    motors.setSpeeds(MAX_SPEED, -MAX_SPEED);
    delay(100);
  }

  motors.setSpeeds(0,0);
  delay(300);
}

void driveForward(short moveTime)
{
  //drive faster
  for (int speed = 0; speed <= MAX_SPEED; speed++)
  {
    motors.setSpeeds(speed, speed);
    delay(30);
  }

  //just drive
  for (int sec = 0; sec <= moveTime; sec++)
  {
    motors.setSpeeds(MAX_SPEED, MAX_SPEED);
    delay(30);
  }

  //drive slower
  for (int speed = MAX_SPEED; speed >= 0; speed--)
  {
    motors.setSpeeds(speed, speed);
    delay(30);
  }
  motors.setSpeeds(0, 0);
}

void loop()
{
  driveForward(5); 
  turnRight(8);
}
