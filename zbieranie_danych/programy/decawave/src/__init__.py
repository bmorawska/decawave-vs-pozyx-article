import numpy as np

FIRST_ANCHOR_POSITION = np.array([0, 0])  # x & y
SECOND_ANCHOR_POSITION = np.array([20, 0])
THIRD_ANCHOR_POSITION = np.array([10, 15])
