from datetime import datetime


def Ranges(success, range0, range1, range2):
    now = datetime.now()
    timestamp = datetime.timestamp(now)

    json = {"timestamp": timestamp,
            "success": success,
            "ranges": [
                        {"range0": range0},
                        {"range1": range1},
                        {"range2": range2},
                      ],
            }
    return json
