import serial
import sys
import datetime
import json

from multicastTransmitter_for_Pi import send
from json_data_send_for_Pi import Ranges

INTERRUPTED_BY_USER = 0
SERIAL_PORT_LINUX = '/dev/ttyACM0'  # if it does not work please type: sudo chmod 666 /dev/ttyACM0 in terminal
BAUDRATE = 9600
MILLIMETERS_PER_METER = 1000

success = False

while True:
    try:
        success = False
        port = serial.Serial(SERIAL_PORT_LINUX, BAUDRATE)
        raw_data = port.readline()
        data = raw_data.split()
        range0, range1, range2 = 0, 0, 0

        if data[0] == b'mc':
            mask = int(data[1], 16)

            if mask & 0x01:
                range0 = int(data[2], 16) / MILLIMETERS_PER_METER
                #print('Anchor 0: {} m'.format(range0))
            else:
                print('Anchor 0 not connected')

            if mask & 0x02:
                range1 = int(data[3], 16) / MILLIMETERS_PER_METER
                #print('Anchor 1: {} m'.format(range1))
            else:
                print('Anchor 1 not connected')

            if mask & 0x04:
                range2 = int(data[4], 16) / MILLIMETERS_PER_METER
                #print('Anchor 2: {} m'.format(range2))
            else:
                print('Anchor 2 not connected')

    except (KeyboardInterrupt, SystemExit):
        print('\nProgram interrupted by user')
        sys.exit(INTERRUPTED_BY_USER)

    if (range0 and range1 and range2) != 0:
        success = True

    msg = Ranges(success, range0, range1, range2)
    json_message = json.dumps(msg)
    send(str.encode(json_message))
