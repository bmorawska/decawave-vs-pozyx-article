import json

jsonData = '[' \
           '    {' \
           '        "version":"1",' \
           '        "alive":true,' \
           '        "tagId":"26459",' \
           '        "success":true,' \
           '        "timestamp":1581431710.838,' \
           '        "data":{' \
           '            "tagData":{' \
           '                "gyro":{' \
           '                    "x":0,' \
           '                    "y":-0.0625,' \
           '                    "z":0.0625},' \
           '                "magnetic":{' \
           '                    "x":-21.375,' \
           '                    "y":-4.375,' \
           '                    "z":-33.75},' \
           '                "quaternion":{' \
           '                    "x":-0.10205078125,' \
           '                    "y":0.11248779296875,' \
           '                    "z":0.65045166015625,' \
           '                    "w":-0.74420166015625},' \
           '                "linearAcceleration":{' \
           '                    "x":-2,' \
           '                    "y":-3,' \
           '                    "z":8},' \
           '                "pressure":0,' \
           '                "maxLinearAcceleration":0' \
           '            },' \
           '            "anchorData":[],' \
           '            "coordinates":{' \
           '                "x":2073,' \
           '                "y":4921,' \
           '                "z":1618}' \
           '            ,"acceleration":{' \
           '                "x":32,' \
           '                "y":288,' \
           '                "z":940},' \
           '            "orientation":{' \
           '                "yaw":1.436,' \
           '                "roll":0.034,' \
           '                "pitch":-0.302},' \
           '            "metrics":{"latency":54.1,"rates":{"update":15.029,"success":15.029}}}}]'

#d = json.loads(jsonData)

file = open('MQTTPozyxJsonToCSV_1', 'w')

with open('pozyxAPI_1.csv') as f:
    content = f.readlines()
    content = [x.strip() for x in content]
    for i in range(len(content)):
        d = json.loads(content[i])

        if (d[0]['data']['coordinates']['x'] and d[0]['data']['coordinates']['y'] and d[0]['data']['coordinates']['z']):
            print(str(d[0]['data']['coordinates']['x']), end = ',')
            print(str(d[0]['data']['coordinates']['y']), end = ',')
            print(str(d[0]['data']['coordinates']['z']))







