import socket
import struct
import json
import time


#################
# Tables with data
#################

time_marker = int(time.time())
filename = 'dataPozyx_2_proba' + str(time_marker) + '.csv'


# file = open('dataPozyx.csv', 'w')
file = open(filename, 'w')
#file.write('timestamp')
#file.write(' ')
#file.write('coordinates - x')
#file.write(' ')
#file.write('coordinates - y')
#file.write(' ')
#file.write('acceleration - x')
#file.write(' ')
#file.write('acceleration - y')
#file.write(' ')
#file.write('acceleration - z')
#file.write('\n')

# Run First!
multicast_group = '239.39.39.39'
server_address = ('0.0.0.0', 5007)

# Create the socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# Bind to the server address
sock.bind(server_address)

# Tell the operating system to add the socket to the multicast group
# on all interfaces.
group = socket.inet_aton(multicast_group)
mreq = struct.pack('4sL', group, socket.INADDR_ANY)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

counter = 0

while True:
    data, address = sock.recvfrom(1024)
    d = json.loads(data)
    try:

        file.write(str(d['timestamp']))
        file.write(',')
        file.write(str(d['coordinates'][0]['x']))
        file.write(',')
        file.write(str(d['coordinates'][0]['y']))
        file.write(',')
        file.write(str(d['acceleration'][0]['x']))
        file.write(',')
        file.write(str(d['acceleration'][0]['y']))
        file.write(',')
        file.write(str(d['acceleration'][0]['z']))
        file.write('\n')

    except KeyboardInterrupt:
        break
    print(d['coordinates'][0]['x'], d['coordinates'][0]['y'])

file.close()


