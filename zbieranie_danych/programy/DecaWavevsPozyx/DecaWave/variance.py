from numpy.linalg import norm
from numpy import array, dot

from src.UWB.globals import ANCH_0, ANCH_1, ANCH_2, sqr


def variance_function(x):
    # function which reflects variance over distance
    y = 0.1
    return y


def variance_vector(anchor, tag):
    vec = anchor - tag
    veclen = norm(vec)
    variance = vec / veclen * variance_function(veclen)
    return array(variance)


def projection_on_axes(vec, x_axis, y_axis):
    ux = dot(vec, x_axis) / sqr(norm(x_axis)) * x_axis
    uy = dot(vec, y_axis) / sqr(norm(y_axis)) * y_axis
    return ux, uy


def mean_variance_on_axes(point):
    x_axis = array(ANCH_1 - ANCH_0)
    y_axis = array([x_axis[1], -x_axis[0]])

    vec_anch1 = variance_vector(ANCH_0, point)
    vec_anch2 = variance_vector(ANCH_1, point)
    vec_anch3 = variance_vector(ANCH_2, point)

    sigma1 = array(projection_on_axes(vec_anch1, x_axis, y_axis))
    sigma2 = array(projection_on_axes(vec_anch2, x_axis, y_axis))
    sigma3 = array(projection_on_axes(vec_anch3, x_axis, y_axis))

    mean_sigma = array(sigma1 + sigma2 + sigma3) / 3

    return mean_sigma

