import socket
import struct
import json
import time

from DecaWave.geometry import count_vertices, count_center

#################
# Tables with data
#################

time_marker = int(time.time())
filename = 'decaWave_static_B' + str(time_marker) + '.csv'
#file = open('dataDecaWave.csv', 'w')
file = open(filename, 'w')
#file.write('timestamp')
#file.write('range0')
#file.write('range1')
#file.write('range2')

# Run Second
multicast_group = '238.38.38.38'
server_address = ('0.0.0.0', 5008)

# Create the socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# Bind to the server address
sock.bind(server_address)

# Tell the operating system to add the socket to the multicast group
# on all interfaces.
group = socket.inet_aton(multicast_group)
mreq = struct.pack('4sL', group, socket.INADDR_ANY)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

counter = 0

while True:
    data, address = sock.recvfrom(1024)
    d = json.loads(data)
    try:
        range0 = d['ranges'][0]['range0']
        range1 = d['ranges'][1]['range1']
        range2 = d['ranges'][2]['range2']
        timestamp = d['timestamp']
        center = ["error", "error"]

        if (range0 and range1 and range2) != 0:
            triangle = count_vertices(range0, range1, range2)
            center = count_center(triangle)

        file.write(str(timestamp))
        file.write(',')
        file.write(str(range0))
        file.write(',')
        file.write(str(range1))
        file.write(',')
        file.write(str(range2))
        file.write(',')
        file.write(str(center[0]))
        file.write(',')
        file.write(str(center[1]))
        file.write('\n')

    except KeyboardInterrupt:
        break

    if counter % 10 == 0:
        print(range0, range1, range2, center)

    counter += 1

file.close()


