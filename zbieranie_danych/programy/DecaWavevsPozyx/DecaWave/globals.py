from numpy import array

# Anchor localization
ANCH_0 = array([0, 0])
ANCH_1 = array([0, 4.0])
ANCH_2 = array([6.0, 2.048])

# Experiment settings
NUMBER_OF_MEASURES = 10000
FILENAME = 'offlinetest.csv'

# Error codes
INTERRUPTED_BY_USER = 0

# Connection settings
SERIAL_PORT_LINUX = '/dev/ttyACM0'  # if it does not work please type: sudo chmod 666 /dev/ttyACM0 in terminal
BAUDRATE = 9600
TIMEOUT = 0

# Constants
MILLIMETERS_PER_METER = 1000


# Functions
def sqr(x):
    return x * x
